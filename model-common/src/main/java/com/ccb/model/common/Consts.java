package com.ccb.model.common;

/**
 * 常量类
 */
public final class Consts {
    /**
     * 斜杠
     */
    public static final String SLASH = "/";
    /**
     * utf-8编码
     */
    public static final String UTF8 = "utf-8";
    /**
     * 空字符串
     */
    public static final String EMPTY = "";
    /**
     * 分号
     */
    public static final String SEMICOLON = ";";
    /**
     * 短横线
     */
    public static final String SHORTLINE = "-";

    /**
     * message字段默认长度阈值
     */
    public static final int MSG_MAXLENGTH = 1024;
    /**
     * 状态message常量
     */
    public static final String SUCCESS = "SUCCESS";
    /**
     * 失败message常量
     */
    public static final String FAILED = "FAILED";
    /**
     * 日期格式
     */
    public static final String DATE_PATTERN = "yyyy-MM-dd";
    /**
     * 日期格式带时分秒
     */
    public static final String FULL_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    /**
     * 应答结果状态码——成功
     */
    public static final int RESULT_CODE_SUCCESS = 0;
    /**
     * 应答结果状态码——通用错误
     */
    public static final int RESULT_CODE_COMMONERR = 9999;
    /**
     * NOT Exists
     */
    public static final int RESULT_CODE_NOTEXIST = 1000;
    /**
     * business error
     */
    public static final int RESULT_CODE_BUSINESSERR = 1100;
    /**
     * 远程调用错误
     */
    public static final int RESULT_CODE_RPCERR = 1200;
    /**
     * session key
     */
    public static final String USER_SESSION_KEY = "user";

    /**
     * 超级管理员
     */
    public static Long SUPER_ADMIN_ID = 1L;

    /**
     * 超级管理员
     */
    public static String SUPER_ADMIN_NAME = "超级管理员";

    /**
     * 员工类型
     */
    public static final int PERSON_TYPE_EMP = 0;
    /**
     * 会员来源
     */
    public static final int PERSON_TYPE_MEM = 1;

    /**
     * 默认地址
     */
    public static final int STATUS_ADDRESS_DEFAULT = 1;
    /**
     * 未默认地址
     */
    public static final int STATUS_ADDRESS_NOT_DEFAULT = 0;

    /**
     * 逻辑已删除
     */
    public static final int STATUS_DELETED_YES = 1;
    /**
     * 逻辑未删除
     */
    public static final int STATUS_DELETED_NO = 0;

    /**
     * excel 2003
     */
    public static final String EXCEL_XLS = "xls";
    /**
     * excel 2007
     */
    public static final String EXCEL_XLSX = "xlsx";

    /**
     * 通用状态有效与无效-有效
     */
    public static final byte STATUS_ENABLED = 1;
    /**
     * 通用状态有效与无效-无效
     */
    public static final byte STATUS_DISABLED = 2;
    /**
     * 文件上传最大3M
     */
    public static final long FILE_UPLOAD_MAX_SIZE = 3 * 1024 * 1024;



}
