package com.ccb.model.api.validation;

import javax.validation.groups.Default;

/**
 * @Description: 简易检验组 (CRUD)
 * @author : wenlin.zhang
 */
public interface Groups{
    /**
     * 默认空组
     */
    Class[] EMPTY = {};
    /**
     * C->Create Group
     */
    interface C extends Default {}
    /**
     * R->Retrieve Group
     */
    interface R extends Default {}
    /**
     * U->Update Group
     */
    interface U extends Default {}
    /**
     * D->Delete Group
     */
    interface D extends Default {}
}
