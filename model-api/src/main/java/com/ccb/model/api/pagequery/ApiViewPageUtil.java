package com.ccb.model.api.pagequery;

import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;

/**
 * Description:分页工具
 * @author wenlin.zhang
 */
public class ApiViewPageUtil {
    /**
     * 默认起始条数
     */
    private static final int DEFAULT_OFFSET = 0;

    /**
     * 默认单页条数
     */
    private static final int DEFAULT_PAGE_SIZE = 1000;

    /**
     * Description: 检查分页参数
     * @param dto 分页参数
     * created by wenlin.zhang
     */
    public static void checkPageArgs(com.ccb.stock.api.pagequery.ApiViewPage dto) {
        Assert.notNull(dto, "参数不能为空");
        if(dto.getPage() != null) {
            // 校验分页参数
            if(dto.getRows() <= 0) {
                throw new IllegalArgumentException("每页条数必须为正整数");
            }
            if(dto.getPage() != null) {
                if(dto.getPage() <= 0) {
                    throw new IllegalArgumentException("页码必须为正整数");
                }
                dto.setOffset((dto.getPage() - 1) * dto.getRows());
            }
            if(dto.getOffset() != null) {
                if(dto.getOffset() < 0) {
                    throw new IllegalArgumentException("起始条数必须为非负整数");
                }
            }
            // 分页参数pageSize必填，offset和pageNo选填其一
            if(dto.getOffset() == null) {
                throw new IllegalArgumentException("分页参数错误");
            }
        } else {
            // 默认不分页，导出数据，限制条数
            dto.setOffset(DEFAULT_OFFSET);
            dto.setRows(DEFAULT_PAGE_SIZE);
        }
    }

    /**
    * Description: 根据分页参数计算起始行号（请先执行checkPageArgs方法校验dto）
    * @param dto 分页参数
    * @return int 起始行号
    * Date: 2017/7/19 15:28 created by 张亮(liang.zhang@ccbcoffee.com)
    */
    public static int getStartIndex(com.ccb.stock.api.pagequery.ApiViewPage dto) {
        return dto.getOffset();
    }

    /**
     * Description: 根据分页参数计算结束行号（请先执行checkPageArgs方法校验dto）
     * @param dto 分页参数
     * @return int 结束行号
     * Date: 2017/7/19 15:28 created by 张亮(liang.zhang@ccbcoffee.com)
     */
    public static int getEndIndex(com.ccb.stock.api.pagequery.ApiViewPage dto) {
        return dto.getOffset() + dto.getRows();
    }

    /**
     * Description: 对list进行分页
     * @param pageDto 分页参数
     * @param list list
     * @param <T> list类型
     * @return 分页后list
     */
    public static <T> List<T> getPageOfList(com.ccb.stock.api.pagequery.ApiViewPage pageDto, List<T> list) {
        checkPageArgs(pageDto);
        Assert.notNull(list, "待分页列表不能为空");
        int start = getStartIndex(pageDto);
        int end = getEndIndex(pageDto);
        int size = list.size();
        if(start >= size) {
            return Collections.EMPTY_LIST;
        } else if(end > size) {
            end = size;
        }
        return list.subList(start, end);
    }
}
