
package com.ccb.stock.api.pagequery;

import java.io.Serializable;

/**
 * @author wenlin.zhang
 */
public abstract class ApiViewPage implements Serializable {
    private Integer offset;
    private Integer page;
    private Integer rows;


    public ApiViewPage() {
    }

    public Integer getOffset() {
        return this.offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getRows() {
        return rows;
    }

    public void setRows(Integer rows) {
        this.rows = rows;
    }

}
