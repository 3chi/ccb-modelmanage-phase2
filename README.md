modelmanagementphase2
======
#Git Commit Message规范：
* FEAT 增删改一个功能
* FIX 修复一个Bug
* DOCS 增删改文档
* STYLE 代码美化或者其它不影响功能的代码修改
* REFACTOR 重构一个功能
* PERF 优化一个功能
* TEST 增删改一个单元测试
* CHORE 框架升级、Jar包引用增删改等

#Git Commit Message例子：
* FEAT 新增模型注册api
* FIX 详情页bug修复
* DOCS 增加3.3.3上线步骤
* STYLE mainService代码格式化
* REFACTOR 模型注册功能重构
* PERF 模型对比页面优化
* TEST 增加模型注册单元测试
* CHORE model-api升级到1.0.23
